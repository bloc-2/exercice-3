package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.message.Message;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * cette interface donne la possibilité d'utiliser des fonctions pour récuperer des objets wallet et badge en accès irect
 */
public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {
    /**
     * méthode pour retourner une liste de métadonées du portefeuille de badges
     * @return
     * @throws IOException
     */
    List<DigitalBadgeMetadata> getWalletMetadata() throws IOException;

    /**
     * methode pour récuperer un badge à partir de ses métadonnées
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;
}
