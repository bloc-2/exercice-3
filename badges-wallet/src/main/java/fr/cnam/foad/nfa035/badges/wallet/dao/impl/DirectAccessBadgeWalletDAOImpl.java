package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.*;
import java.util.List;

/**
 * Cette classe définit des objets agissant sur des badges avec des fonctions d'accès direct sur un fichier walletDatabase
 */
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
   private final File walletDatabase;

   /**
    * constructeur qui définit un fichier par son chemin et l'affecte à l'attribut de classe walletDatabase
    * @param dbPath
    * @throws IOException
    */
   public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
      this.walletDatabase = new File(dbPath);
   }

   /**
    * Cette classe ajoute un badge à patir d'un fichier image dans un walletDataBase
    * @param image
    * @throws Exception
    */
   @Override
   public void addBadge(File image) throws Exception {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {

         ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl();
         serializer.serialize(image, media);
         // A COMPLéTER
      }
   }

   /**
    * cette fonction désérialise le flux d'un badge imageStream vers un WalletFrame media
    * @param imageStream
    * @throws IOException
    */
   @Override
   public void getBadge(OutputStream imageStream) throws IOException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
         new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
   }

   /**
    * cette methode retourne une liste de badges
    * @return
    * @throws FileNotFoundException
    */
   @Override
   public List<DigitalBadgeMetadata> getWalletMetadata() throws FileNotFoundException {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {

         // A COMPLéTER
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
       return null;
   }

   /**
    * je ne comprend pas ce que doit faire cette methode, je ne comprends pas ce que c'est que les metadonnées dans notre projet
    * @param imageStream
    * @param meta
    * @throws FileNotFoundException
    */
   @Override
   public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws FileNotFoundException {
      List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
         // A COMPLéTER
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
   }
}
