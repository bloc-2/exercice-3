package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;

/**
 * cette interface permet de se doter de fonctions pour la désérialisation
 */
public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia> {
    /**
     * permet de désérialiser le contenu d'un walletframemedia vers un digitalbadgemetadata
     * @param media
     * @param meta
     * @throws IOException
     */
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;

    /**
     * retourne l'outputstram source
     * @return
     * @param <T>
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * initialise l'outputstream os
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);
}
