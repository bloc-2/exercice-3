package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.util.List;

/**
 * cette interface permet  de se dote de méthodes pour la désérialisation avec des métadonnées
 */
public interface MetadataDeserializer {
   /**
    * désérialiser
    * @param media
    * @return
    * @throws IOException
    */
   List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException;

   /**
    * désérialiser avec méadonnées
    * @param media
    * @param meta
    * @throws IOException
    */
   void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;
}
