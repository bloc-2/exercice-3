package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * classe qui permet de creer des objets qui ont des méthodes de désérialisation en mode database
 */
public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {
    /**
     * methode qui retourne une liste  de badges à partir d'un WalletFrameMedia
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {
        return null;
    }

    /**
     * methode qui désérialise et écrit le résultat dans un flux outputstream
     * @param media
     * @param meta
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }

    /**
     * je en comprend pas ce que doit faire cette fonction
     * @param datum
     * @return
     */
    private InputStream getDeserializingStream(String datum) {
        return null;
    }

    /**
     * retourne le flux outputstream
     * @return
     */
    private OutputStream getSourceOutputStream() {
        return null;
    }


    /**
     * crée un String avec la derniere ligne du fichier caractere par caractère et dès qu'il voit le symbole de retour à la ligne il s'arrete.
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
