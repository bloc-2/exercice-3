package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * cette classe permet de créer de sobjets avec des méthodes de désérialisation en mode acces direct
 */
public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {
    /**
     * méthode pour désérialiser
     * @param media
     * @param meta
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }

    /**
     * retourne le flux outputstream
     * @return
     * @param <T>
     */
    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return null;
    }

    /**
     * initialiser le flux outputstream
     * @param os
     * @param <T>
     */
    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

    }

    /**
     * méthode permettant de désérialiser
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {

    }

    /**
     * je ne comprend pas ce que doit faire cette méthode
     * @param data
     * @return
     * @param <K>
     * @throws IOException
     */
    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return null;
    }
}
