package fr.cnam.foad.nfa035.badges.wallet.model;

public class DigitalBadgeMetadata {

    int badgeId;
    long imageSize;
    long walletPosition;

    public DigitalBadgeMetadata(int badgeId, long imageSize, long walletPosition) {

        this.badgeId = badgeId;
        this.walletPosition = walletPosition;
        this.imageSize = imageSize;


    }

    public long getWalletPosition() {
        return 0;
    }
}
